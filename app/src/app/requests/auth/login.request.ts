export class LoginRequest {
  public email: string;
  public password: string;
  public device_name: string;

  constructor(data: LoginRequest) {
    this.email = data.email;
    this.password = data.password;
    this.device_name = data.device_name;
  }
}
