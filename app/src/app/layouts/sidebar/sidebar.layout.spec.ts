import { SidebarLayout } from './sidebar.layout';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

describe('SidebarLayout', () => {
  let component: SidebarLayout;
  let fixture: ComponentFixture<SidebarLayout>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SidebarLayout],
      });

      fixture = TestBed.createComponent(SidebarLayout);
      component = fixture.componentInstance;
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
