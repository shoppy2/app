import { Component } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.layout.html',
  styleUrls: ['./sidebar.layout.scss'],
})
export class SidebarLayout {}
