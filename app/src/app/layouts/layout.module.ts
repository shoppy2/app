import { NgModule } from '@angular/core';
import { SidebarLayout } from './sidebar/sidebar.layout';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [IonicModule, RouterModule],
  exports: [SidebarLayout],
  declarations: [SidebarLayout],
  providers: [],
})
export class LayoutModule {}
