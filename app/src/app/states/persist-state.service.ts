import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { ShoppingList } from '@models/shopping-list';
import { AuthQuery } from './auth/auth.query';
import { AuthState, AuthStore } from './auth/auth.store';
import { ShoppingListQuery } from './shopping-list/shopping-list.query';
import { ShoppingListStore } from './shopping-list/shopping-list.store';

@Injectable({
  providedIn: 'root',
})
export class PersistStateService {
  constructor(
    private storage: Storage,
    private shoppingListStore: ShoppingListStore,
    private shoppingListQuery: ShoppingListQuery,
    private authStore: AuthStore,
    private authQuery: AuthQuery
  ) {}

  public async persistShoppingList(): Promise<void> {
    const lists: ShoppingList[] = await this.storage.get('shopping_lists');
    this.shoppingListStore.set(lists);

    this.shoppingListQuery.selectAll().subscribe(async (selectedLists) => {
      await this.storage.set('shopping_lists', selectedLists);
    });
  }

  public async persistAuth(): Promise<void> {
    const auth: AuthState = await this.storage.get('auth');
    this.authStore.update(auth);

    this.authQuery.select().subscribe(async (auth) => {
      await this.storage.set('auth', auth);
    });
  }
}
