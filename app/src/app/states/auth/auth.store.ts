import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { User } from '@models/user';

export interface AuthState {
  user: User | undefined;
  token: string;
  isAuthenticated: boolean;
}

const createInitialState = (): AuthState => ({
  user: undefined,
  token: '',
  isAuthenticated: false,
});

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'auth' })
export class AuthStore extends Store<AuthState> {
  constructor() {
    super(createInitialState());
  }
}
