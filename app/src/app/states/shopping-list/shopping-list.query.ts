import { QueryEntity } from '@datorama/akita';
import { ShoppingListState, ShoppingListStore } from './shopping-list.store';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ShoppingListQuery extends QueryEntity<ShoppingListState> {
  constructor(store: ShoppingListStore) {
    super(store);
  }
}
