import {
  ActiveState,
  EntityState,
  EntityStore,
  StoreConfig,
} from '@datorama/akita';
import { ShoppingList } from '@models/shopping-list';
import { Injectable } from '@angular/core';
import { ShoppingListIdType } from '@models/types/shopping-list-id.type';

export interface ShoppingListState
  extends EntityState<ShoppingList, ShoppingListIdType>,
    ActiveState {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'shopping-list' })
export class ShoppingListStore extends EntityStore<ShoppingListState> {
  constructor() {
    super();
  }
}
