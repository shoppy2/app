import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import * as CordovaSQLiteDriver from 'localforage-cordovasqlitedriver';
import { PersistStateService } from '@states/persist-state.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private storage: Storage,
    private persistStateService: PersistStateService
  ) {}

  async ngOnInit() {
    await this.storage.create();
    await this.storage.defineDriver(CordovaSQLiteDriver);
    await this.persistStateService.persistShoppingList();
    await this.persistStateService.persistAuth();
  }
}
