import { TestBed } from '@angular/core/testing';
import { IAuthService } from '@services/api/auth.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { User } from '@models/user';
import { environment } from '@environments/environment';
import { AuthStore } from '@states/auth/auth.store';

describe('AuthService', () => {
  let service: IAuthService;
  const httpSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj(
    'HttpClient',
    ['post', 'get']
  );
  const authStoreSpy: jasmine.SpyObj<AuthStore> = jasmine.createSpyObj(
    'AuthStore',
    ['update']
  );
  const user: User = {
    id: 1,
    name: 'Test',
    email: 'test@example.com',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpSpy },
        { provide: AuthStore, useValue: authStoreSpy },
      ],
    });

    service = TestBed.inject(IAuthService);
  });

  it('logs in an user', (done) => {
    const loginDetails = { email: 'test@example.com', password: 'password' };
    const authResponse = { data: { user, token: '1234' } };
    httpSpy.post
      .withArgs(`${environment.apiUrl}/login`, loginDetails)
      .and.returnValue(of(authResponse));

    const response = service.login(loginDetails);

    response.subscribe((res) => {
      expect(res).toEqual({ user, token: '1234' });
      done();
    });
  });

  it('sets the authenticated user after login', (done) => {
    const loginDetails = { email: 'test@example.com', password: 'password' };
    const authResponse = { data: { user, token: '1234' } };
    httpSpy.post.and.returnValue(of(authResponse));

    service.login(loginDetails).subscribe(() => {
      expect(authStoreSpy.update).toHaveBeenCalledWith({
        user,
        token: '1234',
        isAuthenticated: true,
      });

      done();
    });
  });
});
