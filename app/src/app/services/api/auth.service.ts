import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SingleResponse } from '@responses/single.response';
import { LoginRequest } from '@requests/auth/login.request';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '@services/http-service';
import { map, tap } from 'rxjs/operators';
import { AuthStore } from '@states/auth/auth.store';
import { Auth } from '@models/auth';

@Injectable()
class AuthService extends HttpService implements IAuthService {
  constructor(private http: HttpClient, private authStore: AuthStore) {
    super();
  }

  public login(data: LoginRequest): Observable<Auth> {
    return this.http
      .post<SingleResponse<Auth>>(`${this.apiUrl}/login`, data)
      .pipe(
        map((response: SingleResponse<Auth>) => response.data),
        tap((auth) =>
          this.authStore.update({
            user: auth.user,
            token: auth.token,
            isAuthenticated: true,
          })
        )
      );
  }
}

@Injectable({
  providedIn: 'root',
  useClass: AuthService,
})
export abstract class IAuthService {
  public abstract login(data: LoginRequest): Observable<Auth>;
}
