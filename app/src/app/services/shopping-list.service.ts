import { Injectable } from '@angular/core';
import { ShoppingList } from '@models/shopping-list';
import { ShoppingListStore } from '../states/shopping-list/shopping-list.store';
import { ShoppingListQuery } from '../states/shopping-list/shopping-list.query';
import { ShoppingListIdType } from '@models/types/shopping-list-id.type';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ShoppingListService {
  public shoppingLists$ = this.shoppingListQuery.selectAll();

  constructor(
    private shoppingListStore: ShoppingListStore,
    private shoppingListQuery: ShoppingListQuery
  ) {}

  public select(list: ShoppingList): void {
    this.shoppingListStore.setActive(list.id);
  }

  public active(): ShoppingList | undefined {
    return this.shoppingListQuery.getActive();
  }

  public getList(id: number | string): Observable<ShoppingList | undefined> {
    return this.shoppingListQuery.selectEntity(id);
  }

  public storeLocal(list: ShoppingList): void {
    this.shoppingListStore.add(list);
  }

  public addProduct(id: ShoppingListIdType, product: string): void {
    this.shoppingListStore.update(id, (entity) => ({
      ...entity,
      items: [
        ...entity.items,
        {
          product,
          amount: 0,
        },
      ],
    }));
  }
}
