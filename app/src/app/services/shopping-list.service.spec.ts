import { TestBed } from '@angular/core/testing';

import { ShoppingListService } from './shopping-list.service';
import { ShoppingListStore } from '../states/shopping-list/shopping-list.store';
import { ShoppingListQuery } from '../states/shopping-list/shopping-list.query';
import { Storage } from '@ionic/storage-angular';
import { of } from 'rxjs';
import { createList } from '../spec-helpers/model.spec-helper';

describe('ShoppingListService', () => {
  let service: ShoppingListService;
  let shoppingListStoreSpy: jasmine.NonTypedSpyObj<ShoppingListStore>;
  let shoppingListQuerySpy: jasmine.SpyObj<ShoppingListQuery>;
  let storageSpy: jasmine.SpyObj<Storage>;

  beforeEach(() => {
    shoppingListStoreSpy = jasmine.createSpyObj('shoppingListStore', [
      'add',
      'set',
      'setActive',
      'update',
    ]);
    shoppingListQuerySpy = jasmine.createSpyObj('shoppingListQuery', [
      'selectAll',
      'getActive',
      'selectEntity',
    ]);
    storageSpy = jasmine.createSpyObj('storage', ['set', 'get']);

    TestBed.configureTestingModule({
      providers: [
        { provide: ShoppingListStore, useValue: shoppingListStoreSpy },
        { provide: ShoppingListQuery, useValue: shoppingListQuerySpy },
        { provide: Storage, useValue: storageSpy },
        ShoppingListService,
      ],
    });

    TestBed.inject(ShoppingListStore);
    service = TestBed.inject(ShoppingListService);
  });

  it('can store a shopping list locally', () => {
    const list = createList();
    service.storeLocal(list);

    expect(shoppingListStoreSpy.add.calls.count()).toBe(1);
  });

  it('can persist shopping list in storage', async () => {
    const list = createList();
    shoppingListQuerySpy.selectAll.and.returnValue(of([list]));

    await service.persist();

    expect(shoppingListStoreSpy.set.calls.count()).toBe(1);
    expect(storageSpy.set.calls.count()).toBe(1);
  });

  it('could select a shopping list', () => {
    const list = createList();
    service.select(list);

    expect(shoppingListStoreSpy.setActive).toHaveBeenCalled();
  });

  it('could get the active shopping list', () => {
    service.active();
    expect(shoppingListQuerySpy.getActive).toHaveBeenCalled();
  });

  it('could get a shopping list', () => {
    service.getList(0);
    expect(shoppingListQuerySpy.selectEntity).toHaveBeenCalled();
  });

  it('could add a product to a shopping list', () => {
    const list = createList();
    service.addProduct(list.id, 'Test product');

    expect(shoppingListStoreSpy.update).toHaveBeenCalled();
  });
});
