import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import { ShoppingList } from '@models/shopping-list';
import { ShoppingListService } from '@services/shopping-list.service';
import { ModalController } from '@ionic/angular';
import { AddProductComponent } from '@pages/shopping-lists/add-product/add-product.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  public list: ShoppingList | undefined;

  constructor(
    private route: ActivatedRoute,
    private shoppingListService: ShoppingListService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.getShoppingList();
  }

  public async addProduct(): Promise<void> {
    const modal = await this.modalController.create({
      component: AddProductComponent,
      componentProps: {
        shoppingList: this.list,
      },
    });

    await modal.present();

    modal.onDidDismiss().then(() => {
      this.getShoppingList();
    });
  }

  private getShoppingList(): void {
    const activeList = this.shoppingListService.active();

    if (activeList) {
      this.list = activeList;
      return;
    }

    this.route.params
      .pipe(
        map((params) => params.id),
        switchMap((id) =>
          this.shoppingListService.getList(id).pipe(
            switchMap((list) => {
              if (!list) {
                // TODO: Get shopping list from api

                return EMPTY;
              }

              return of(list);
            })
          )
        )
      )
      .subscribe((list) => (this.list = list));
  }
}
