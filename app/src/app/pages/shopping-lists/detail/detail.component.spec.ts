import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';

import { DetailComponent } from './detail.component';
import { ShoppingListService } from '@services/shopping-list.service';
import { createList } from '../../../spec-helpers/model.spec-helper';
import { of } from 'rxjs';
import { findEl } from '../../../spec-helpers/element.spec-helper';
import { ActivatedRoute } from '@angular/router';

describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>;
  let shoppingListServiceSpy: jasmine.SpyObj<ShoppingListService>;
  let modalControllerSpy: jasmine.SpyObj<ModalController>;
  let routeSpy: jasmine.SpyObj<ActivatedRoute>;

  beforeEach(
    waitForAsync(() => {
      shoppingListServiceSpy = jasmine.createSpyObj('ShoppingListService', [
        'active',
        'getList',
      ]);
      modalControllerSpy = jasmine.createSpyObj('ModalController', [
        'create',
        'present',
      ]);
      routeSpy = jasmine.createSpyObj('ActivatedRoute', ['params']);

      TestBed.configureTestingModule({
        declarations: [DetailComponent],
        imports: [IonicModule.forRoot()],
        providers: [
          { provide: ShoppingListService, useValue: shoppingListServiceSpy },
          { provide: ModalController, useValue: modalControllerSpy },
          { provide: ActivatedRoute, useValue: routeSpy },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(DetailComponent);
      component = fixture.componentInstance;
    })
  );

  it('gets the details of an active shopping list', () => {
    const list = createList();
    shoppingListServiceSpy.active.and.returnValue(list);

    fixture.detectChanges();

    expect(component.list).toBe(list);
  });

  it('gets the details of an shopping list by route params', () => {
    const list = createList();
    shoppingListServiceSpy.active.and.returnValue(undefined);
    shoppingListServiceSpy.getList.and.returnValue(of(list));
    routeSpy.params = of({
      id: 0,
    });

    fixture.detectChanges();

    expect(component.list).toBe(list);
  });

  it('opens the add product modal when the add product button is clicked', () => {
    const modalSpy: jasmine.SpyObj<HTMLIonModalElement> = jasmine.createSpyObj(
      'Modal',
      ['present', 'onDidDismiss']
    );
    const addProductButton = findEl(fixture, 'add-product-button');
    modalControllerSpy.create.and.returnValue(Promise.resolve(modalSpy));
    modalSpy.onDidDismiss.and.returnValue(Promise.resolve({ data: undefined }));
    shoppingListServiceSpy.active.and.returnValue(createList());

    addProductButton.triggerEventHandler('click', null);

    expect(modalControllerSpy.create).toHaveBeenCalled();

    fixture.whenStable().then(() => {
      expect(modalSpy.present).toHaveBeenCalled();
    });
  });
});
