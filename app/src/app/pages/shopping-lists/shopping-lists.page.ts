import { Component, OnDestroy, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CreateComponent } from '@pages/shopping-lists/create/create.component';
import { ShoppingList } from '@models/shopping-list';
import { takeUntil } from 'rxjs/operators';
import { ShoppingListService } from '@services/shopping-list.service';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shopping-lists',
  templateUrl: './shopping-lists.page.html',
  styleUrls: ['./shopping-lists.page.scss'],
})
export class ShoppingListsPage implements OnInit, OnDestroy {
  public shoppingLists!: ShoppingList[];
  private destroy = new Subject();

  constructor(
    private modalController: ModalController,
    private shoppingListService: ShoppingListService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getShoppingLists();
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.complete();
  }

  public async createList(): Promise<void> {
    const modal = await this.modalController.create({
      component: CreateComponent,
    });

    return await modal.present();
  }

  public goToList(list: ShoppingList): void {
    this.shoppingListService.select(list);
    this.router.navigate(['shopping-lists', list.id]);
  }

  private getShoppingLists(): void {
    this.shoppingListService.shoppingLists$
      .pipe(takeUntil(this.destroy))
      .subscribe((lists) => (this.shoppingLists = lists));
  }
}
