import { ShoppingListsPage } from '@pages/shopping-lists/shopping-lists.page';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';
import { ShoppingListService } from '@services/shopping-list.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { findEl } from '../../spec-helpers/element.spec-helper';
import { createList } from '../../spec-helpers/model.spec-helper';
import { Router } from '@angular/router';

describe('ShoppingListPage', () => {
  // let component: ShoppingListsPage;
  let fixture: ComponentFixture<ShoppingListsPage>;
  let shoppingListServiceSpy: jasmine.SpyObj<ShoppingListService>;
  let modalControllerSpy: jasmine.SpyObj<ModalController>;
  let routerSpy: jasmine.SpyObj<Router>;

  beforeEach(
    waitForAsync(() => {
      shoppingListServiceSpy = jasmine.createSpyObj('shoppingListService', [
        'storeLocal',
        'select',
      ]);
      modalControllerSpy = jasmine.createSpyObj('ModalController', ['create']);
      routerSpy = jasmine.createSpyObj('Router', ['navigate']);

      TestBed.configureTestingModule({
        declarations: [ShoppingListsPage],
        imports: [IonicModule],
        providers: [
          { provide: ShoppingListService, useValue: shoppingListServiceSpy },
          { provide: ModalController, useValue: modalControllerSpy },
          { provide: Router, useValue: routerSpy },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(ShoppingListsPage);
      // component = fixture.componentInstance;
    })
  );

  it('fetches the shopping lists', () => {
    shoppingListServiceSpy.shoppingLists$ = of([
      { id: 0, name: 'Test 1', items: [], local: true },
      { id: 0, name: 'Test 2', items: [], local: true },
    ]);

    fixture.detectChanges();

    const nameSelectors = fixture.debugElement.queryAll(By.css('ion-label h2'));

    expect(nameSelectors[0].nativeElement.textContent).toBe('Test 1');
    expect(nameSelectors[1].nativeElement.textContent).toBe('Test 2');
  });

  it('opens create list modal after create button is clicked', async () => {
    const modalSpy: jasmine.SpyObj<HTMLIonModalElement> = jasmine.createSpyObj(
      'Modal',
      ['present']
    );
    modalControllerSpy.create.and.returnValue(Promise.resolve(modalSpy));

    const createButton = findEl(fixture, 'create-btn');

    createButton.triggerEventHandler('click', null);

    expect(modalControllerSpy.create).toHaveBeenCalled();

    fixture.whenStable().then(() => {
      expect(modalSpy.present).toHaveBeenCalled();
    });
  });

  it('navigates to the details of a list when a list is clicked', () => {
    shoppingListServiceSpy.shoppingLists$ = of([createList()]);

    fixture.detectChanges();

    const listItem = findEl(fixture, 'list-item');

    listItem.triggerEventHandler('click', null);

    expect(shoppingListServiceSpy.select).toHaveBeenCalled();
    expect(routerSpy.navigate).toHaveBeenCalledWith(['shopping-lists', 0]);
  });
});
