import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingListsPage } from './shopping-lists.page';
import { DetailComponent } from '@pages/shopping-lists/detail/detail.component';

const routes: Routes = [
  {
    path: '',
    component: ShoppingListsPage,
  },
  {
    path: ':id',
    component: DetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShoppingListsPageRoutingModule {}
