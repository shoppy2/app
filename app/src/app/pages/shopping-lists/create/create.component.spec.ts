import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';

import { CreateComponent } from './create.component';
import { ShoppingListService } from '@services/shopping-list.service';
import { findEl } from '../../../spec-helpers/element.spec-helper';

describe('CreateComponent', () => {
  let component: CreateComponent;
  let fixture: ComponentFixture<CreateComponent>;
  let shoppingListServiceSpy: jasmine.SpyObj<ShoppingListService>;
  let modalControllerSpy: jasmine.SpyObj<ModalController>;

  beforeEach(
    waitForAsync(() => {
      shoppingListServiceSpy = jasmine.createSpyObj('ShoppingListService', [
        'storeLocal',
      ]);
      modalControllerSpy = jasmine.createSpyObj('ModalController', ['dismiss']);

      TestBed.configureTestingModule({
        declarations: [CreateComponent],
        imports: [IonicModule.forRoot()],
        providers: [
          { provide: ShoppingListService, useValue: shoppingListServiceSpy },
          { provide: ModalController, useValue: modalControllerSpy },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(CreateComponent);
      component = fixture.componentInstance;
    })
  );

  it('closes the modal when the close button is clicked', () => {
    const closeButton = findEl(fixture, 'closeBtn');

    closeButton.triggerEventHandler('click', null);

    expect(modalControllerSpy.dismiss).toHaveBeenCalled();
  });

  it('can create an shopping list', () => {
    fixture.detectChanges();
    const saveBtn = findEl(fixture, 'saveBtn');

    component.nameControl.setValue('Test name');
    saveBtn.triggerEventHandler('click', null);

    expect(shoppingListServiceSpy.storeLocal).toHaveBeenCalled();

    fixture.whenStable().then(() => {
      expect(modalControllerSpy.dismiss).toHaveBeenCalled();
    });
  });
});
