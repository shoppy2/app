import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormControl, Validators } from '@angular/forms';
import { ShoppingList } from '@models/shopping-list';
import { ShoppingListService } from '@services/shopping-list.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  public nameControl: FormControl;

  constructor(
    private modalController: ModalController,
    private shoppingListService: ShoppingListService
  ) {
    this.nameControl = new FormControl('', Validators.required);
  }

  ngOnInit() {}

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async save(): Promise<void> {
    const shoppingList: ShoppingList = {
      id: uuidv4(),
      name: this.nameControl.value,
      items: [],
      local: true,
    };

    this.shoppingListService.storeLocal(shoppingList);

    await this.close();
  }
}
