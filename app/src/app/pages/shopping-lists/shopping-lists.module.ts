import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ShoppingListsPageRoutingModule } from './shopping-lists-routing.module';
import { ShoppingListsPage } from './shopping-lists.page';
import { CreateComponent } from '@pages/shopping-lists/create/create.component';
import { DetailComponent } from '@pages/shopping-lists/detail/detail.component';
import { AddProductComponent } from '@pages/shopping-lists/add-product/add-product.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    ShoppingListsPageRoutingModule,
  ],
  declarations: [
    ShoppingListsPage,
    CreateComponent,
    DetailComponent,
    AddProductComponent,
  ],
})
export class ShoppingListsPageModule {}
