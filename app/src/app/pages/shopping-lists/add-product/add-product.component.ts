import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormControl, Validators } from '@angular/forms';
import { ShoppingListService } from '@services/shopping-list.service';
import { ShoppingList } from '@models/shopping-list';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss'],
})
export class AddProductComponent implements OnInit {
  @Input() shoppingList!: ShoppingList;
  public productControl: FormControl;

  constructor(
    private modalController: ModalController,
    private shoppingListService: ShoppingListService
  ) {
    this.productControl = new FormControl('', Validators.required);
  }

  ngOnInit() {}

  public async close(): Promise<void> {
    await this.modalController.dismiss();
  }

  public async save(): Promise<void> {
    if (this.productControl.invalid) {
      return;
    }

    this.shoppingListService.addProduct(
      this.shoppingList.id,
      this.productControl.value
    );
    await this.close();
  }
}
