import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';

import { AddProductComponent } from './add-product.component';
import { findEl } from '../../../spec-helpers/element.spec-helper';
import { ShoppingListService } from '@services/shopping-list.service';
import { createList } from '../../../spec-helpers/model.spec-helper';

describe('AddProductComponent', () => {
  let component: AddProductComponent;
  let fixture: ComponentFixture<AddProductComponent>;
  let modalControllerSpy: jasmine.SpyObj<ModalController>;
  let shoppingListServiceSpy: jasmine.SpyObj<ShoppingListService>;

  beforeEach(
    waitForAsync(() => {
      modalControllerSpy = jasmine.createSpyObj('ModalController', ['dismiss']);
      shoppingListServiceSpy = jasmine.createSpyObj('ShoppingListService', [
        'addProduct',
      ]);

      TestBed.configureTestingModule({
        declarations: [AddProductComponent],
        imports: [IonicModule.forRoot()],
        providers: [
          { provide: ModalController, useValue: modalControllerSpy },
          { provide: ShoppingListService, useValue: shoppingListServiceSpy },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(AddProductComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('closes the modal when the close button is clicked', () => {
    const closeButton = findEl(fixture, 'close-button');
    closeButton.triggerEventHandler('click', null);

    expect(modalControllerSpy.dismiss).toHaveBeenCalled();
  });

  it('adds the given product to the shopping list', () => {
    const saveButton = findEl(fixture, 'saveButton');
    component.productControl.setValue('Test product');
    component.shoppingList = createList();

    saveButton.triggerEventHandler('click', null);

    expect(shoppingListServiceSpy.addProduct).toHaveBeenCalledWith(
      0,
      'Test product'
    );

    fixture.whenStable().then(() => {
      expect(modalControllerSpy.dismiss).toHaveBeenCalled();
    });
  });
});
