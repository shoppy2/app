import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync,
} from '@angular/core/testing';
import { IonicModule, LoadingController } from '@ionic/angular';

import { LoginComponent } from './login.component';
import { findEl } from '@spec-helpers/element.spec-helper';
import { Router } from '@angular/router';
import { IAuthService } from '@services/api/auth.service';
import { of } from 'rxjs';
import { LoginRequest } from '@requests/auth/login.request';
import { createUser } from '@spec-helpers/model.spec-helper';
import { ReactiveFormsModule } from '@angular/forms';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let loadingSpy: jasmine.SpyObj<HTMLIonLoadingElement>;
  const loadingControllerSpy: jasmine.SpyObj<LoadingController> =
    jasmine.createSpyObj('LoadingController', ['create']);
  const routerSpy: jasmine.SpyObj<Router> = jasmine.createSpyObj('Router', [
    'navigate',
  ]);
  const authServiceSpy: jasmine.SpyObj<IAuthService> = jasmine.createSpyObj(
    'IAuthService',
    ['login']
  );

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [LoginComponent],
        imports: [IonicModule.forRoot(), ReactiveFormsModule],
        providers: [
          { provide: LoadingController, useValue: loadingControllerSpy },
          { provide: Router, useValue: routerSpy },
          { provide: IAuthService, useValue: authServiceSpy },
        ],
      }).compileComponents();

      loadingSpy = jasmine.createSpyObj('Loading', ['present', 'dismiss']);
      loadingControllerSpy.create.and.returnValue(Promise.resolve(loadingSpy));
      authServiceSpy.login.and.returnValue(
        of({
          user: createUser(),
          token: '',
        })
      );

      fixture = TestBed.createComponent(LoginComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should login the user when the login button is clicked', fakeAsync(() => {
    const loginButton = findEl(fixture, 'login-btn');
    component.form.get('email')?.setValue('test@example.com');
    component.form.get('password')?.setValue('password');

    loginButton.triggerEventHandler('click', null);

    tick();

    expect(loadingControllerSpy.create).toHaveBeenCalled();

    fixture.whenStable().then(() => {
      expect(loadingSpy.present).toHaveBeenCalled();
    });

    expect(authServiceSpy.login).toHaveBeenCalledWith(
      new LoginRequest({
        email: 'test@example.com',
        password: 'password',
        device_name: 'default',
      })
    );

    fixture.whenStable().then(() => {
      expect(loadingSpy.dismiss).toHaveBeenCalled();
      expect(routerSpy.navigate).toHaveBeenCalledWith(['shopping-lists']);
    });
  }));
});
