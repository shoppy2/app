import { Component, OnInit } from '@angular/core';
import { IAuthService } from '@services/api/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ToastController } from '@ionic/angular';
import { LoginRequest } from '@requests/auth/login.request';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Device } from '@capacitor/device';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public form!: FormGroup;

  constructor(
    private authService: IAuthService,
    private formBuilder: FormBuilder,
    private loadingController: LoadingController,
    private router: Router,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.initForm();
  }

  public async login(): Promise<void> {
    const loading = await this.loadingController.create();
    await loading.present();

    const deviceInfo = await Device.getInfo();

    const request = new LoginRequest({
      ...this.form.value,
      device_name: deviceInfo.name ?? 'default',
    });

    this.authService.login(request).subscribe({
      next: async () => {
        await loading.dismiss();
        await this.router.navigate(['shopping-lists']);
      },
      error: async (err: HttpErrorResponse) => {
        await loading.dismiss();
        this.clearPasswordField();

        if (err.status === 422) {
          const toast = await this.toastController.create({
            message: err.error.errors[Object.keys(err.error.errors)[0]][0],
            duration: 3000,
          });
          toast.present();
        } else {
          const toast = await this.toastController.create({
            message: 'Er ging iets mis. Probeer het later opnieuw',
            duration: 3000,
          });
          toast.present();
        }
      },
    });
  }

  private initForm(): void {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
    });
  }

  private clearPasswordField(): void {
    this.form.get('password')?.setValue('');
  }
}
