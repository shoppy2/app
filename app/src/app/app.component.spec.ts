import { AppComponent } from './app.component';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Storage } from '@ionic/storage-angular';
import { ShoppingListService } from '@services/shopping-list.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(
    waitForAsync(() => {
      const ionicStorageSpy = jasmine.createSpyObj('Storage', [
        'create',
        'defineDriver',
      ]);
      const shoppingListServiceSpy = jasmine.createSpyObj(
        'ShoppingListService',
        ['persist']
      );

      TestBed.configureTestingModule({
        declarations: [AppComponent],
        providers: [
          { provide: Storage, useValue: ionicStorageSpy },
          { provide: ShoppingListService, useValue: shoppingListServiceSpy },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(AppComponent);
      component = fixture.componentInstance;
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
