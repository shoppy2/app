import { ShoppingListItem } from '@models/shopping-list-item';
import { ShoppingListIdType } from '@models/types/shopping-list-id.type';

export interface ShoppingList {
  id: ShoppingListIdType;
  name: string;
  items: ShoppingListItem[];
  local: boolean;
}
