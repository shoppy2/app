import { User } from '@models/user';

export interface Auth {
  user: User;
  token: string;
}
