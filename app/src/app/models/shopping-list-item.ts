export interface ShoppingListItem {
  product: string;
  amount: number;
}
