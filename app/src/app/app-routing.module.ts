import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { SidebarLayout } from './layouts/sidebar/sidebar.layout';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'shopping-lists',
    pathMatch: 'full',
  },
  {
    path: 'shopping-lists',
    component: SidebarLayout,
    loadChildren: () =>
      import('./pages/shopping-lists/shopping-lists.module').then(
        (m) => m.ShoppingListsPageModule
      ),
  },
  {
    path: 'auth',
    component: SidebarLayout,
    loadChildren: () =>
      import('./pages/auth/auth.module').then((m) => m.AuthModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
