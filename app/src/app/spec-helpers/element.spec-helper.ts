import { ComponentFixture } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

/**
 * Return a selector for the `data-testid` attribute with the given attribute value.
 *
 * @param testId
 */
export const testIdSelector = (testId: string): string =>
  `[data-testid="${testId}"]`;

/**
 * Finds a single element inside the Component by the given css selector.
 * Throws an error if no element was found.
 *
 * @param fixture
 * @param selector
 */
export const queryByCss = <T>(
  fixture: ComponentFixture<T>,
  selector: string
): DebugElement => {
  const debugElement = fixture.debugElement.query(By.css(selector));

  if (!debugElement) {
    throw new Error(`queryByCss: Element with ${selector} not found`);
  }

  return debugElement;
};

/**
 * Finds an element inside the Component by the given `data-testid` attribute.
 * Throws an error if no element was found.
 *
 * @param fixture
 * @param testId
 */
export const findEl = <T>(
  fixture: ComponentFixture<T>,
  testId: string
): DebugElement => queryByCss(fixture, testIdSelector(testId));

/**
 * Finds all elements with the given `data-testid` attribute.
 *
 * @param fixture
 * @param testId
 */
export const findEls = <T>(
  fixture: ComponentFixture<T>,
  testId: string
): DebugElement[] =>
  fixture.debugElement.queryAll(By.css(testIdSelector(testId)));

/**
 * Gets the text content of an element with the given `data-testid` attribute.
 *
 * @param fixture
 * @param testId
 */
export const getText = <T>(
  fixture: ComponentFixture<T>,
  testId: string
): string => findEl(fixture, testId).nativeElement.textContent;

/**
 * Expects that the element with the given `data-testid` attribute has the given text content.
 *
 * @param fixture
 * @param testId
 * @param text
 */
export const expectText = <T>(
  fixture: ComponentFixture<T>,
  testId: string,
  text: string
): void => {
  expect(getText(fixture, testId)).toBe(text);
};
