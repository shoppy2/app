import { ShoppingList } from '@models/shopping-list';
import { User } from '@models/user';

export const createList = (): ShoppingList => ({
  id: 0,
  name: 'Test',
  items: [],
  local: true,
});

export const createUser = (): User => ({
  id: 0,
  name: 'Test user',
  email: 'test@example.com',
});
