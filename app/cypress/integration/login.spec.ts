describe('Login', () => {
  beforeEach(() => {
    indexedDB.deleteDatabase('_ionicstorage');
  });

  it('can login a user', () => {
    cy.visit('auth/login');
    cy.get('[data-testid="email"]').type('jacob@example.com');
    cy.get('[data-testid="password"]').type('password');
    cy.get('[data-testid="login-btn"]').click();
    cy.url().should('contain', 'shopping-lists');
  });

  it('shows an error when a user tries to login with the wrong credentials', () => {
    cy.visit('auth/login');
    cy.get('[data-testid="email"]').type('jacob@example.com');
    cy.get('[data-testid="password"]').type('wrong');
    cy.get('[data-testid="login-btn"]').click();
    cy.url().should('contain', 'auth/login');
  });
});
