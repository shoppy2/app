describe('ShoppingList', () => {
  beforeEach(() => {
    indexedDB.deleteDatabase('_ionicstorage');
  });

  it('can create a shopping list and add a product', () => {
    cy.visit('shopping-lists');
    cy.get('[data-testid="create-btn"]').click();
    cy.contains('Maak lijst aan');
    cy.get('[data-testid="nameInput"]').click().type('Test lijst');
    cy.get('[data-testid="saveBtn"]').click();
    cy.contains('Mijn lijsten');
    cy.contains('Test lijst');
    cy.get('[data-testid="list-item"]').click();
    cy.contains('Test lijst');
    cy.get('[data-testid="add-product-button"]').click();
    cy.contains('Voeg product toe');
    cy.get('[data-testid="productInput"]').click().type('Test product');
    cy.get('[data-testid="saveButton"]').click();
    cy.contains('Test lijst');
    cy.contains('Test product');
  });
});
