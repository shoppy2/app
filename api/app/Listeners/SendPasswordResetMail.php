<?php

namespace App\Listeners;

use App\Notifications\ResetPasswordNotification;
use Illuminate\Auth\Events\PasswordReset;

class SendPasswordResetMail
{
    public function handle(PasswordReset $event): void
    {
        $event->user->notify(new ResetPasswordNotification());
    }
}
