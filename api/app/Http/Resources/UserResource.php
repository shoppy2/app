<?php

namespace App\Http\Resources;

use App\Models\User;

class UserResource extends BaseResource
{
    public function format(User $resource)
    {
        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'email' => $resource->email,
        ];
    }
}
