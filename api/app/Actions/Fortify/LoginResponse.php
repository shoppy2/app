<?php

namespace App\Actions\Fortify;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{
    public function toResponse($request)
    {
        Validator::make($request->all(), [
            'device_name' => 'required|string',
        ])->validate();

        /** @var User $user */
        $user = auth()->user();

        $token = $user->createToken($request->device_name)->plainTextToken;

        return response()->json([
            'data' => [
                'user' => UserResource::make($user),
                'token' => $token,
            ],
        ]);
    }
}
