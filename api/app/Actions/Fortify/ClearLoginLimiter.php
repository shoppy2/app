<?php

namespace App\Actions\Fortify;

use Laravel\Fortify\LoginRateLimiter;

class ClearLoginLimiter
{
    protected LoginRateLimiter $limiter;

    public function __construct(LoginRateLimiter $limiter)
    {
        $this->limiter = $limiter;
    }

    public function handle($request, $next)
    {
        $this->limiter->clear($request);

        return $next($request);
    }
}
