<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
        $this->withoutMiddleware(EnsureFrontendRequestsAreStateful::class);
    }

    public function authenticated(string $guard = 'sanctum'): User
    {
        $user = User::factory()->create([
            'name' => 'Test user',
        ]);

        $this->actingAs($user, $guard);

        return $user;
    }
}
