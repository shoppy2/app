<?php

namespace Tests\Feature;

use App\Models\User;
use App\Notifications\ForgotPasswordNotification;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Illuminate\Testing\TestResponse;
use Tests\RefreshDatabase;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_login_a_user(): void
    {
        $this->useSession();

        /** @var User $user */
        $user = User::factory()->create([
            'email' => 'test@example.com',
        ]);

        $this->postLogin(['email' => 'test@example.com', 'password' => 'password', 'device_name' => 'test'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'user',
                    'token',
                ],
            ])
            ->assertJson([
                'data' => [
                    'user' => [
                        'name' => $user->name,
                        'email' => $user->email,
                    ],
                ],
            ]);

        $this->assertAuthenticatedAs($user, 'sanctum');
    }

    /** @test */
    public function it_fails_when_an_employee_logs_in_with_invalid_credentials(): void
    {
        $this->useSession();
        $this->withExceptionHandling();

        User::factory()->create([
            'email' => 'test@example.com',
        ]);

        $this->postLogin(['email' => 'test@example.com', 'password' => 'wrong'])
            ->assertStatus(422);
    }

    /** @test */
    public function it_can_log_a_user_out(): void
    {
        $this->useSession();
        $this->authenticated('web');

        $this->postJson(route('logout'))
            ->assertStatus(204);

        self::assertFalse($this->isAuthenticated(), 'The user is authenticated');
    }

    /** @test */
    public function it_can_send_a_forgot_password_mail(): void
    {
        Notification::fake();

        $user = User::factory()->create();

        $this->postJson(route('password.email'), ['email' => $user->email])
            ->assertStatus(200);

        Notification::assertSentTo([$user], ForgotPasswordNotification::class, function ($notification) use ($user) {
            return Hash::check($notification->token, DB::table('password_resets')->where('email', $user->email)->latest()->first()->token);
        });
    }

    /** @test */
    public function it_can_reset_a_user_password(): void
    {
        Notification::fake();

        $user = User::factory()->create([
            'password' => 'old_password',
        ]);

        $token = hash_hmac('sha256', Str::random(40), config('app.key'));

        DB::table('password_resets')->insert([
            'email' => $user->email,
            'token' => Hash::make($token),
        ]);

        $this->postJson(route('password.update'), [
            'email' => $user->email,
            'token' => $token,
            'password' => 'new_password',
            'password_confirmation' => 'new_password',
        ])
            ->assertStatus(200);

        $this->assertTrue(Hash::check('new_password', $user->refresh()->password));

        Notification::assertSentTo($user, ResetPasswordNotification::class);
    }

    /** @test */
    public function it_can_register_a_user(): void
    {
        $this->useSession();

        $this->postJson('api/register', [
            'name' => 'Test',
            'email' => 'test@example.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ])
            ->assertStatus(201);

        $user = User::latest()->first();

        $this->assertSame('Test', $user->name);
        $this->assertSame('test@example.com', $user->email);
        $this->assertTrue(Hash::check('password', $user->password));
    }

    protected function postLogin(array $request): TestResponse
    {
        return $this->postJson('api/login', $request);
    }

    protected function useSession(): void
    {
        $this->app[Kernel::class]->pushMiddleware(StartSession::class);
    }
}
